from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from polls.models import Question,Choice
from django.template import loader
from django.http import Http404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # template = loader.get_template('polls/index.html')
    output = ', '.join([q.question_text for q in latest_question_list])
    context={
        'latest_question_list':latest_question_list,
    }
    # return HttpResponse(template.render(context, request))
    return render(request,'polls/index.html',context)


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    context = {
        'question': question ,
    }
    return render(request,'polls/detail.html',context)

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

def form(request,question_id):
    question = get_object_or_404(Question,pk=question_id)

    try:
        selected_choice =  question.choice_set.get(pk=request.POST['choice'])

    except(KeyError, Choice.DoesNotExist):
        context={
        'question':question,
        'error_message': "You didn't select a choice.",
        }
        return render(request,'polls/form.html',context)
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

class IndexView(ListView):
   
    template_name = "polls/index.html"
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]
    